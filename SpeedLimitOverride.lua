-- speedLimitOverride specialization for FS22
-- Author: sperrgebiet
---
-- Unlike others I see the meaning in mods that others can learn, expand and improve them. So feel free to use this in your own mods, 
-- add stuff to it, improve it. Your own creativity is the limit ;) If you want to mention me in the credits fine. If not, I'll live happily anyway :P
-- Yeah, I know. I should do a better job in document my code... Next time, I promise... 

SpeedLimitOverride = {}
SpeedLimitOverride.eventName = {}
SpeedLimitOverride.ModName = g_currentModName
SpeedLimitOverride.ModDirectory = g_currentModDirectory
SpeedLimitOverride.Version = "1.0.0.2"

SpeedLimitOverride.debug = fileExists(SpeedLimitOverride.ModDirectory ..'debug')

--print(string.format('SpeedLimitOverride v%s - DebugMode %s)', SpeedLimitOverride.Version, tostring(SpeedLimitOverride.debug)))

addModEventListener(SpeedLimitOverride)

function SpeedLimitOverride:dp(val, fun, msg) -- debug mode, write to log
	if not SpeedLimitOverride.debug then
		return;
	end

	if msg == nil then
		msg = ' ';
	else
		msg = string.format(' msg = [%s] ', tostring(msg));
	end

	local pre = 'SpeedLimitOverride DEBUG:';

	if type(val) == 'table' then
		--if #val > 0 then
			print(string.format('%s BEGIN Printing table data: (%s)%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
			DebugUtil.printTableRecursively(val, '.', 0, 3);
			print(string.format('%s END Printing table data: (%s)%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
		--else
		--	print(string.format('%s Table is empty: (%s)%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
		--end
	else
		print(string.format('%s [%s]%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
	end
end

function SpeedLimitOverride.prerequisitesPresent(specializations)
    return true
end


function SpeedLimitOverride.registerEventListeners(vehicleType)
	local functionNames = {	"onRegisterActionEvents", "onPostLoad", "saveToXMLFile", "onReadStream", "onWriteStream" }
	--local functionNames = {	"onRegisterActionEvents", "onPostLoad", "saveToXMLFile" }
	
	for _, functionName in ipairs(functionNames) do
		SpecializationUtil.registerEventListener(vehicleType, functionName, SpeedLimitOverride)
	end
end

function SpeedLimitOverride:onRegisterActionEvents(isSelected, isOnActiveVehicle)

	if self.isClient then
		local spec = self.spec_speedLimitOverride
		self:clearActionEventsTable(spec.actionEvents)
	
		if isSelected then
			-- Just add events if we get a valid speedLimit from the selected vehicle/implement
			if SpeedLimitOverride:getCurrentSpeedlimit() ~= nil then
				spec.actionEvents = {}
				local actions = {
					"speedLimit_Increase",
					"speedLimit_Decrease",
					"speedLimit_Reset"
					}

				for _, action in pairs(actions) do
					local actionMethod = string.format("action_%s", action);
					local _, actionEventId = self:addActionEvent(spec.actionEvents, action, self, SpeedLimitOverride[actionMethod], false, true, false, true, nil)
					g_inputBinding:setActionEventTextPriority(actionEventId, GS_PRIO_NORMAL)
					g_inputBinding:setActionEventActive(actionEventId, true)
					g_inputBinding:setActionEventTextVisibility(actionEventId, SpeedLimitOverride.showHelp)
				end
			end
		end
	end
end

function SpeedLimitOverride:action_speedLimit_Increase(actionName, keyStatus, arg3, arg4, arg5)
    SpeedLimitOverride:dp("Limit increase")
	local object = g_currentMission.controlledVehicle.currentSelection.object.vehicle
    if object.speedLimit ~= nil then
		if object.speedLimit < SpeedLimitOverride.maxSpeed then
			local curSpeedLimit = SpeedLimitOverride:getCurrentSpeedlimit(object)
			SpeedLimitOverride:setCurrentSpeedLimit(curSpeedLimit + 1)
		else
			SpeedLimitOverride:setCurrentSpeedLimit(1)
		end
    end
end

function SpeedLimitOverride:action_speedLimit_Decrease(actionName, keyStatus, arg3, arg4, arg5)
    SpeedLimitOverride:dp("Limit decrease")
	local object = g_currentMission.controlledVehicle.currentSelection.object.vehicle
    if object.speedLimit ~= nil then
		if object.speedLimit > 1 then
			local curSpeedLimit = SpeedLimitOverride:getCurrentSpeedlimit(object)
			SpeedLimitOverride:setCurrentSpeedLimit(curSpeedLimit - 1)
		else
			SpeedLimitOverride:setCurrentSpeedLimit(SpeedLimitOverride.maxSpeed)
		end		
    end
end

function SpeedLimitOverride:action_speedLimit_Reset(actionName, keyStatus, arg3, arg4, arg5)
	SpeedLimitOverride:dp("Limit reset")

    local storeLimit = tonumber(Vehicle.loadSpecValueSpeedLimit(g_currentMission.controlledVehicle.currentSelection.object.vehicle.xmlFile))
    SpeedLimitOverride:dp(string.format("storeLimit %s", storeLimit))

    if SpeedLimitOverride:getCurrentSpeedlimit() ~= storeSpeedlimit then
        SpeedLimitOverride:setCurrentSpeedLimit(storeLimit)
    end
end

-- can't use onDraw, as it would render everything twice then, in case of an e.g. combine
function SpeedLimitOverride:draw(isActiveForInput, isActiveForInputIgnoreSelection, isSelected)
    if g_currentMission.hud.isVisible and g_currentMission.controlledVehicle ~= nil then
        -- We just want to show the speedlimit info for devices where it make sense. So no harvestes without attached cutter e.g.
		-- Not nice, but I can't think about a better option for now.
		if g_currentMission.controlledVehicle.currentSelection ~= nil and g_currentMission.controlledVehicle.currentSelection.object ~= nil and g_currentMission.controlledVehicle.currentSelection.object.vehicle.spec_workArea ~= nil and  g_currentMission.controlledVehicle.currentSelection.object.vehicle.typeName ~= "combineDrivable" then
			local speed = SpeedLimitOverride:getCurrentSpeedlimit()
			if speed ~= nil then
				SpeedLimitOverride:printInfotext(speed)
			end
		end
    end
end

function SpeedLimitOverride:onPostLoad(savegame)
	SpeedLimitOverride.initSpecialization()

	if savegame ~= nil then
		local limit = savegame.xmlFile:getValue(savegame.key..".speedLimitOverride#limit")
		if limit ~= nil then
			self.speedLimit = limit
			SpeedLimitOverride:dp(string.format("Loaded speed limit: %d", limit))
		end
	end
end

function SpeedLimitOverride:saveToXMLFile(xmlFile, key)
    -- Some vehicles have inf (infinite) as speedLimit, hence to check if that's the case
    if (self.spec_speedLimitOverride ~= nil) and (self.speedLimit ~= nil) and (self.speedLimit > -math.huge and self.speedLimit < math.huge) then
        if tonumber(Vehicle.loadSpecValueSpeedLimit(self.xmlFile)) ~= tonumber(self.speedLimit) then
            SpeedLimitOverride:dp(string.format("saveToXMLFile: Vehicle {%s} speedLimit: %d", self.configFileName, self.speedLimit))
			xmlFile:setValue(key.."#limit", self.speedLimit)
        end
    end
end

function SpeedLimitOverride.initSpecialization()
	local schema = Vehicle.xmlSchemaSavegame
	schema:setXMLSpecializationType("speedLimitOverride")

	schema:register(XMLValueType.INT, "vehicles.vehicle(?).speedLimitOverride#limit", "")
	
	
	if SpeedLimitOverride.xmlSchema == nil then
		local configSchema = XMLSchema.new("SpeedLimitOverride")
		configSchema.rootNodeName = "SpeedLimitOverride"
		configSchema:setXMLSpecializationType("speedLimitOverride")
		configSchema:register(XMLValueType.BOOL, ".showHelp", "Show key bindings in F1 Menu")
		configSchema:register(XMLValueType.INT, ".maxSpeed", "Max speed for a vehicle. Default is 42")
		
		SpeedLimitOverride.xmlSchema = configSchema
	end
	
	if SpeedLimitOverride.xmlFilename == nil then
		local userPath = getUserProfileAppPath()
		createFolder(userPath .. 'modSettings/')
		SpeedLimitOverride.xmlFilename = userPath .. 'modSettings/SpeedLimitOverride.xml'	
		
		--If there is no config we want to create the vanilla one first, and then we're going to load it
		SpeedLimitOverride:writeConfig()
		SpeedLimitOverride:loadConfig()
	end	
end

function SpeedLimitOverride:getCurrentSpeedlimit(object)
	-- We need that to use one method for normal and NW
	local speedLimit = nil
	if object ~= nil then
		speedLimit = object.speedLimit
	elseif g_client ~= nil then
		-- We just want to show the speedlimit if it makes sense for the current vehicle
		if g_currentMission.controlledVehicle ~= nil and g_currentMission.controlledVehicle.currentSelection.object ~= nil then
			-- Always get the speed limit for the current selection
			speedLimit = g_currentMission.controlledVehicle.currentSelection.object.vehicle.speedLimit
		end
	end
	
	if (speedLimit ~= nil) and (speedLimit > -math.huge and speedLimit < math.huge) and (speedLimit > 0) then
		return speedLimit
	end	
end

function SpeedLimitOverride:setCurrentSpeedLimit(speed)
    if g_currentMission.controlledVehicle ~= nil and g_currentMission.controlledVehicle.currentSelection.object.vehicle ~= nil then
         g_currentMission.controlledVehicle.currentSelection.object.vehicle.speedLimit = speed

		 SpeedLimitOverrideEvent.sendEvent(g_currentMission.controlledVehicle.currentSelection.object.vehicle, speed, false)
    end
end

function SpeedLimitOverride:printInfotext(speed)
    local unit = "km/h"
    if g_gameSettings.useMiles then
        unit = "mph"
    end
    g_currentMission:addExtraPrintText(g_i18n.modEnvironments[self.ModName].texts.speedLimitCurrent .. string.format(": %d %s",  speed, unit))
end

function SpeedLimitOverride:loadConfig()
	if g_client ~= nil then
		SpeedLimitOverride:dp("", "loadConfig","")
		if fileExists(SpeedLimitOverride.xmlFilename) then
			--local xmlFile = loadXMLFile('SpeedLimitOverride.loadFile', self.xmlFilename)
			local xmlFile = XMLFile.load('SpeedLimitOverride', self.xmlFilename, SpeedLimitOverride.xmlSchema)
			
			if xmlFile:hasProperty('SpeedLimitOverride') then
				SpeedLimitOverride:dp("", "loadConfig","xmlFile hasProperty true")
				
				-- First see if we should show the key bindings in the help menu
				SpeedLimitOverride.showHelp = xmlFile:getValue("SpeedLimitOverride.showHelp", true)
				SpeedLimitOverride.maxSpeed = xmlFile:getValue("SpeedLimitOverride.maxSpeed", 42)
			end
		end
	end
end

function SpeedLimitOverride:writeConfig()
	if g_client ~= nil then
		-- This is just to create an example config so that it can be altered easier
		if not fileExists(self.xmlFilename) then
			self.saveFile = createXMLFile('SpeedLimitOverride.config', self.xmlFilename, "SpeedLimitOverride")
			setXMLString(self.saveFile, "SpeedLimitOverride.showHelp", "true")
			setXMLInt(self.saveFile, "SpeedLimitOverride.maxSpeed", 42)
			saveXMLFile(self.saveFile)
		end
	end
end


-- MP Events

function SpeedLimitOverride:onReadStream(streamId, connection)
	self.speedLimit = streamReadInt8(streamId)
end

function SpeedLimitOverride:onWriteStream(streamId, connection)
	streamWriteInt8(streamId, SpeedLimitOverride:getCurrentSpeedlimit(self))
end

SpeedLimitOverrideEvent = {}
local SpeedLimitOverrideEvent_mt = Class(SpeedLimitOverrideEvent, Event)
InitEventClass(SpeedLimitOverrideEvent, "SpeedLimitOverrideEvent")

function SpeedLimitOverrideEvent.emptyNew()
    local self = Event.new(SpeedLimitOverrideEvent_mt)
	self.className = "SpeedLimitOverrideEvent"
    return self
end

function SpeedLimitOverrideEvent.new(vehicle, speed)
    local self = SpeedLimitOverrideEvent:emptyNew()
    self.vehicle = vehicle
	self.speed = speed
    return self
end

function SpeedLimitOverrideEvent:writeStream(streamId, connection)
	NetworkUtil.writeNodeObject(streamId, self.vehicle)
	streamWriteInt8(streamId, self.speed)
end

function SpeedLimitOverrideEvent:readStream(streamId, connection)
	self.vehicle = NetworkUtil.readNodeObject(streamId)
	self.speed  = streamReadInt8(streamId)
	self:run(connection)
end

function SpeedLimitOverrideEvent:run(connection)
	self.vehicle.speedLimit = self.speed
end

function SpeedLimitOverrideEvent.sendEvent(vehicle, speed, noEventSend)
	if noEventSend == nil or noEventSend == false then
		if g_server ~= nil then
			g_server:broadcastEvent(SpeedLimitOverrideEvent.new(vehicle, speed), nil, nil, vehicle)
		else
			g_client:getServerConnection():sendEvent(SpeedLimitOverrideEvent.new(vehicle, speed))
		end
	end
end